#! /usr/bin/env python
# -*- coding: utf-8; indent-tabs-mode: nil; python-indent: 2 -*-

import os, glob

import Task,TaskGen,Node
import misc
from TaskGen import *
import pproc

# the following two variables are used by the target "waf dist"
VERSION='0.1'
APPNAME='gjs-tutorial'

# these variables are mandatory ('/' are converted automatically)
srcdir = '.'
blddir = 'build'

def set_options(opt):
  pass

def configure(conf):
  conf.check_tool('gcc gnome misc')
  conf.check_cfg(package='clutter-0.8', args='--cflags --libs', uselib_store='CLUTTER')
  conf.check_cfg(package='gobject-introspection-1.0', args='--cflags --libs', uselib_store='GI')
  conf.check_cfg(package='gjs-gi-1.0', args='--cflags --libs', uselib_store='GJS', mandatory=True) 
  print "Checking for fortran...just kidding!"

def build(bld):
  mainapp = bld.new_task_gen('cc', 'program')
  mainapp.name = 'mainapp'
  mainapp.find_sources_in_dirs('src')
  mainapp.uselib='CLUTTER GI GJS'
  mainapp.target='mainapp'

  # This creates a barrier - easier than trying to make the .gir build depend on the
  # binary.
  bld.add_group()
  
  tutgir = bld.new_task_gen(name='tutgir',
                            source=glob.glob('src/tut-*'),
                            target='Tut-1.0.gir',
                            # Hardcoding ./default/mainapp here is dirty, need to figure out
                            # how to get the complete path
                            rule='g-ir-scanner --namespace=Tut --nsversion=1.0'
                                  ' --include=Clutter-0.8 --pkg=clutter-0.8 --program=./default/mainapp'
                                  ' ${SRC} -o ${TGT}')
                                  
  tut_typelib = bld.new_task_gen(name='tut_typelib',
                                 after='tutgir',
                                 source='Tut-1.0.gir',
                                 target='Tut-1.0.typelib',
                                 rule='g-ir-compiler ${SRC} -o ${TGT}')
