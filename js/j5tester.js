/* j5tester.js - main j5tester module with unit test objects
 *
 * This file is licensed under the MIT License
 *
 * Copyright (c) 2010 John (J5) Palmieri; Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
 
var startsWith = function(cmp_str, value) {
    if (value.substring(0, cmp_str.length) == cmp_str)
        return true;
    
    return false;
}

var error = {
    Failure: function(message) {
        this.name = 'jester.Failure';
        this.message = 'Test failed';
        if (message)
            this.message += ':' + message;
    }
}

var Test = function(test_map, indent) {
    this.init = function() {
        this._indent = '   ';
        if(indent)
           this._indent = indent;
        this.setUpPage = test_map['setUpPage'];
        this.tearDownPage = test_map['tearDownPage'];
        this.test_map = test_map;
        this._indent_level = 0;
    };
    
    this.run = function() {
        for(var t in this.test_map)
           this.run_testsuite(t, this.test_map[t]);
    };

    this.run_testsuite = function(label, suite) {
        this.info('**** ' + label + ' ****\n\n');
        this.indent();
        if (this.setUpPage)
            this.setUpPage();
            
        for(var t in suite)
            if (startsWith('test', t))
                this.run_test(t.substring(t.length), 
                              suite[t],
                              suite['setUp'],
                              suite['tearDown']);
       
        if (this.tearDownPage)
            this.tearDownPage();
            
        this.unindent();
    }; 
    
    this.run_test = function(label, test, setUp, tearDown) {
        this.info('running ' + label + ' test ... ');
        this.indent();
        if(setUp)
            setUp();
        
        test.call(this);
        
        if(tearDown)
            tearDown();
        this.unindent();  
    };
    
    this.gen_indent_str = function() {
        return new Array(this._indent_level).join(this._indent);
    };
    
    this.warn = function() {
        arguments[0] = this.gen_indent_str() + arguments[0];
        printerr.apply(this, arguments);
    };
    
    this.debug = function() {
        log.apply(this, arguments);
    };
    
    this.info = function() {
        arguments[0] = this.gen_indent_str() + arguments[0];
        print.apply(this, arguments);
    };
    
    this.inform = this.info;
    
    this.assert = function(booleanValue, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + booleanValue.toString() + ' is true ... ';
            
        this.info(comment);
        
        if(!booleanValue)
            throw new error.Failure();
            
        this.info('pass');
    };
    
    this.assertTrue = this.assert;
    
    this.assertFalse = function(booleanValue, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + booleanValue.toString() + ' is false ... ';
            
        this.assert(booleanValue == false, comment);
    };
    
    this.assertEquals = function(value1, value2, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value1 + ' == ' + value2 + ' ... ';
            
        this.assert(value1 == value2, comment);
    };
    
    this.assertNotEquals = function(value1, value2, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value1.toString() + ' != ' + value2.toString; + ' ... ';
            
        this.assert(value1 != value2, comment);
    };
    
    this.assertNull = function(value, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value.toString() + ' == (null)';
            
        this.assert(value == null, comment);
    };
    
    this.assertNotNull = function(value, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value.toString() + ' != (null)';
            
        this.assert(value != null, comment);
    };
    
    this.assertUndefined = function(value, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value.toString() + ' is undefined';
            
        this.assert(typeof(value) == 'undefined', comment);
    };
    
    this.assertNotUndefined = function(value, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value.toString() + ' is defined';
            
        this.assert(typeof(value) != 'undefined', comment);
    };
    
    this.assertNaN = function(value, comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value.toString() + ' is not a number';
            
        this.assert(isNaN(value), comment);
    };
    
    this.assertNotNaN = function(value, comment)  {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value.toString() + ' is a number';
            
        this.assert(!isNaN(value), comment);
    };
    
    this.assertTypeOf = function(value, type, comment) {
        var val_type = typeof(value);
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value + ' is of type ' + type;
            
        this.assert(val_type == type, comment);
    };
    
    this.assertNotTypeOf = function(value, type, comment) {
        var val_type = typeof(value);
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Checking if ' + value + ' is not of type ' + type;
            
        this.assert(val_type != type, comment);
    };
    
    this.fail = function(comment) {
        if(comment === null || typeof(comment) == 'undefined')
            comment = 'Failing';
            
        this.assert(false, comment);
    }; 
    
    this.indent = function() { this._indent_level++; };
    this.unindent = function() {
        this._indent_level--;
        if (this._indent_level < 0)
            this._indent_level = 0;
    };
    
    this.init();
}
