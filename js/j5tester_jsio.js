/* j5tester_jsio.js - thin wrapper around jsio that sets up the jsio env
 *
 * This file is licensed under the MIT License
 *
 * Copyright (c) 2010 John (J5) Palmieri; Red Hat, Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
//jsio.js must be in your module path
eval_script('jsio');
jsio.setEnv({Gio: imports.gi.Gio,
             global: GLOBAL,
             getCwd: function() { return __j5tester_cwd; },
             getPath: function() { return __j5tester_path; },
             eval: function(src, path) { return eval(src); },
             log: print,
             findModule: function(possibilities) {
                 for (var i = 0, possible; possible = possibilities[i]; ++i) {
                     var f = this.Gio.file_new_for_path(possible.filePath);
                     var t = f.query_file_type(0, null);
                     if(t == this.Gio.FileType.REGULAR) {
                       var [success, contents, len, etag] = f.load_contents(null);
                           possible.src = contents;
                           return possible;
                     }
                
                 }
             }
            });

