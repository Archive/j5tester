/* main.c - entry point
 *
 * Copyright 2010 John (J5) Palmieri; Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <girepository.h>
#include <gjs/gjs.h>
#include <gjs/jsapi-util.h>
#include <stdlib.h>
#include <string.h>
#include <config.h>

#include <glib.h>
#include <glib/gprintf.h>
#include <glib/gstdio.h>

static char **user_search_path, **global_search_path;
gint is_strict;

static GOptionEntry entries[] =
{
  { "search_path", 'p', 0, G_OPTION_ARG_STRING_ARRAY, &user_search_path, "Appends a path to the search path", NULL},
  { "strict", 0, 0, G_OPTION_ARG_INT, &is_strict, "Determines if we are parsing in strict mode" },
  { NULL }
};

static char **
concat_search_paths(char **default_path, 
                    char *script_path, 
                    char **user_path)
{
  char **search_path;
  int dp_len, up_len = 0, final_len, i, j;

  search_path = g_strdupv(default_path);
  if (search_path == NULL)
    return NULL;

  dp_len = g_strv_length(search_path);

  if (user_path != NULL)
    up_len = g_strv_length(user_path);

  final_len = dp_len + up_len + 1;

  search_path = g_realloc(search_path, 
                          (final_len + 1) * sizeof(char *));

  if (search_path == NULL)
    return NULL;

  search_path[dp_len] = g_strdup(script_path);
  if(search_path[dp_len] == NULL)
    {
      g_strfreev(search_path);
      return NULL;
    }

  search_path[final_len] = NULL;
  for(i = dp_len + 1, j=0; i < final_len; i++, j++)
    {
      search_path[i] = g_strdup(user_path[j]);
      if(search_path[i] == NULL)
        {
          g_strfreev(search_path);
          return NULL;
        }
    }

  return search_path;
}

/*
 * Works like a script tag (with only local file access)
 * finds the file in the search path and evals it
 *
 */
static gboolean
eval_script(JSContext *context,
       JSObject  *obj,
       uintN      argc,
       jsval     *argv,
       jsval     *retval) 
{
    char *path, *search_path;
    char *script_name;
    
    int status;
    GError *error = NULL;
    GjsContext *gjs_context = (GjsContext *)JS_GetContextPrivate(context);
    
    int i = 0;
    search_path = global_search_path[i];
    
    // FIXME: throw an exception
    if(argc != 1)
      return FALSE;
      
    if(!JSVAL_IS_STRING(argv[0]))
      return FALSE;
      
    script_name = g_strconcat(gjs_string_get_ascii(argv[0]), ".js", NULL);
    
    while(search_path != NULL)
      {
        path = g_build_filename(search_path, script_name, NULL);

        if (!path) 
          {
            g_critical ("jstester: Out of memory error");
            exit(1);    
          }
          
        if (g_file_test(path, G_FILE_TEST_IS_REGULAR)) 
          {
            if (!gjs_context_eval_file (gjs_context,
                path,   
                &status,
                &error))
              {
                g_warning ("Evaling script '%s' failed: %s", path, error->message);
                g_error_free (error);
                g_free(path);
                g_free(script_name);
                return FALSE;
              }
            
            g_free(path);
            g_free(script_name);
            return TRUE;      
          }
          
        g_free(path);
        i++;
        search_path = global_search_path[i];
      }
      
    g_free(script_name);
    return FALSE;
}

static JSObject *
search_path_to_js_obj(JSContext *ctx)
{
  JSObject *result;
  jsint i;
  char *path;
  
  result = JS_NewArrayObject(ctx, 0, NULL);
  JS_AddRoot(ctx, result);
  for(i = 0, path = global_search_path[0]; path != NULL; ++i, path = global_search_path[i])
    {
      JS_DefineElement(ctx, result, i,
                       STRING_TO_JSVAL(JS_NewString(ctx, path, strlen(path))),
                       NULL, NULL, JSPROP_READONLY);
    }
    
  JS_RemoveRoot(ctx, result);
  return result; 
}

int 
main(int argc, char *argv[])
{
  GOptionContext *ctx;
  GError *error = NULL;
  int status;
  GjsContext *gjs_ctx;
  JSContext *jsctx;
  
  char *default_search_path[] = {SRCJSDIR, JSDIR, NULL};
  char *exec_script, *exec_dir, *cwd;

  g_type_init();

  ctx = g_option_context_new (NULL); 
  g_option_context_add_main_entries (ctx, entries, GETTEXT_PACKAGE);
  g_option_context_add_group (ctx, g_irepository_get_option_group ());  
  

  if (!g_option_context_parse (ctx, &argc, &argv, &error))
    {
      g_critical ("jstester: %s\n", error->message);
      exit(1);
    }  

  if(argc == 1)
    {
      g_critical ("jstester: no script was provided\n");
      exit(1);
    }

  exec_script = argv[1];
  if (!g_file_test(exec_script, G_FILE_TEST_IS_REGULAR)) 
    {
      g_critical ("jstester: error executing script '%s'\n", exec_script);
      exit(1);
    }

  exec_dir = g_path_get_dirname(exec_script);
  if (exec_dir == NULL)
    {
      g_critical ("jstester: Out of memory error");
      exit(1);
    }

  global_search_path = concat_search_paths(default_search_path, exec_dir, user_search_path);
  if (global_search_path == NULL)
    {
      g_critical ("jstester: Out of memory error");
      exit(1);
    }
 
  g_irepository_prepend_search_path (JSTESTER_PKGLIBDIR);
  
  gjs_ctx = gjs_context_new_with_search_path (global_search_path);
  jsctx = gjs_context_get_native_context(gjs_ctx);  

  if (!is_strict)
    JS_SetOptions(jsctx, JS_GetOptions(jsctx) & ~JSOPTION_STRICT);
  
  // load the main srcipt into the global context
  // FIXME: how do we determine weather to grab the development or installed
  //        system global file?
  if (!gjs_context_eval_file (gjs_ctx,
                         SRCJSDIR"/j5tester_globals.js",
                         &status,
                         &error))
    {
      g_warning ("Evaling globals failed: %s", error->message);
      g_error_free (error);
    }

  if (!JS_DefineFunction(jsctx, JS_GetGlobalObject(jsctx),
                           "eval_script",
                           eval_script,
                           1, GJS_MODULE_PROP_FLAGS))
    {
      g_warning("Failed to define eval_script function");
      return EXIT_FAILURE;
    }
    
  if (!JS_DefineProperty(jsctx, JS_GetGlobalObject(jsctx),
                         "__j5tester_path",
                         OBJECT_TO_JSVAL(search_path_to_js_obj(jsctx)),
                         NULL, NULL, JSPROP_READONLY))
    {
      g_warning("Failed to define j5tester_path property");
      return EXIT_FAILURE;
    }
    
  cwd = g_get_current_dir();
  if (!JS_DefineProperty(jsctx, JS_GetGlobalObject(jsctx),
                         "__j5tester_cwd",
                         STRING_TO_JSVAL(JS_NewString(jsctx, cwd, strlen(cwd))),
                         NULL, NULL, JSPROP_READONLY))
    {
      g_warning("Failed to define j5tester_cwd property");
      return EXIT_FAILURE;
    }

  if (!gjs_context_eval_file (gjs_ctx,
                         exec_script,	
                         &status,
                         &error))
    {
      g_warning ("Evaling script '%s' failed: %s", exec_script, error->message);
      g_error_free (error);
    }
  
  g_strfreev(global_search_path);
  return EXIT_SUCCESS;

}
